import UIKit
import GoogleMaps
import Alamofire

class TruckMapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UIGestureRecognizerDelegate {
    
    let truckMapView = TruckMapView()
    let truckMapViewModel = TruckMapViewModel()
    let geocoder = GMSGeocoder()
    let locationManager = CLLocationManager()
    
    var firstMapUpdate = true

    var mapView: GMSMapView!
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    
    var infoWindow = UIView()
    var tappedMarker = GMSMarker()
    var truckImage = UIImage()
    var truckDescription = ""
    var truckAddress = ""
    var truckName = ""
    var checkinExpiry = ""
    var truckStatus = ""
    var truckCoordinates: CLLocationCoordinate2D!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        truckMapViewModel.delegate = self
        truckMapView.initializeTruckMapView(view: view)
        
        mapView = truckMapView.addMapView(parentView: view)
        mapView!.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 20.0
            locationManager.requestLocation()
        }
        
        let buttonImage = UIImage(named: "Info")
        let infoButton = UIButton(frame: CGRect(x: 50, y: 50, width: 30, height: 30))
        infoButton.setImage(buttonImage, for: .normal)
        infoButton.addTarget(self, action: #selector(TruckMapViewController.didTapInfoButton), for:.touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(infoButton)
        
        [
            infoButton.heightAnchor.constraint(equalToConstant: 30),
            infoButton.widthAnchor.constraint(equalToConstant: 30),
            infoButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            infoButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20)
        ].forEach{ $0.isActive = true }

    }
    
    @objc func didTapInfoButton() {
        let infoPageController = InfoPageController()
        self.navigationController?.pushViewController(infoPageController, animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func updateMap(lastLocation: CLLocation) {
        if firstMapUpdate {
            firstMapUpdate = false
            mapView.camera = GMSCameraPosition.init(latitude: lastLocation.coordinate.latitude,
                                                    longitude: lastLocation.coordinate.longitude,
                                                    zoom: 13.0)
        }
    }
    
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        let lastLocation = locations.last!
        updateMap(lastLocation: lastLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        let southWest = mapView.projection.visibleRegion().nearLeft
        let northEast = mapView.projection.visibleRegion().farRight
        truckMapViewModel.getTruckLocations(southWest: southWest, northEast: northEast)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            infoWindow.center = mapView.projection.point(for: tappedMarker.position)
            infoWindow.center.y = infoWindow.center.y + mapView.frame.height * 0.07
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        tappedMarker = marker
        infoWindow.removeFromSuperview()
        
        let profileImage = UIImageView()
        profileImage.layer.cornerRadius = 10
        profileImage.layer.masksToBounds = true
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.delegate = self
        let truckNameView = UITextField()
        truckNameView.text = marker.title
        truckNameView.font = UIFont(name:"OpenSans-Regular", size:11)
        truckNameView.translatesAutoresizingMaskIntoConstraints = false
        
        let chevronRight = UIImageView()
        chevronRight.image = UIImage(named: "chevron-right-white")
        chevronRight.translatesAutoresizingMaskIntoConstraints = false
        chevronRight.tintColor = .lightGray
        chevronRight.alpha = 0.7
        
        let checkedinAt = UITextField()
        checkedinAt.text = "Here until:"
        checkedinAt.textColor = .gray
        checkedinAt.font = UIFont(name:"OpenSans-Regular", size:9)
        checkedinAt.translatesAutoresizingMaskIntoConstraints = false
        
        let checkinExpiryField = UITextField()
        checkinExpiryField.textColor = .gray
        checkinExpiryField.font = UIFont(name:"OpenSans-Regular", size:9)
        checkinExpiryField.translatesAutoresizingMaskIntoConstraints = false
        
        infoWindow = UIView(frame: CGRect(x: 0, y: 0, width: mapView.frame.width * 0.55, height: mapView.frame.height * 0.1))
        let trianglePath = UIBezierPath()
        trianglePath.move(to:
            CGPoint(x: (infoWindow.frame.maxX * 0.46), y: (infoWindow.frame.minY + (infoWindow.frame.maxY * 0.01)))
        )
        trianglePath.addLine(to:
            CGPoint(x: (infoWindow.frame.maxX * 0.54), y: (infoWindow.frame.minY + (infoWindow.frame.maxY * 0.01)))
        )
        trianglePath.addLine(to:
            CGPoint(x: infoWindow.frame.midX, y: ((infoWindow.frame.minY * 0.99) - (infoWindow.frame.maxY * 0.1)))
        )
        trianglePath.addLine(to:
            CGPoint(x: (infoWindow.frame.maxX * 0.46), y: (infoWindow.frame.minY + (infoWindow.frame.maxY * 0.01)))
        )
        trianglePath.close()
        let shape = CAShapeLayer()
        shape.fillColor = UIColor.white.cgColor
        shape.path = trianglePath.cgPath
        shape.shadowColor = UIColor.black.withAlphaComponent(0.60).cgColor
        shape.shadowOffset = CGSize(width: 0, height: -1)
        shape.shadowRadius = 0.5
        shape.shadowOpacity = 0.2
        infoWindow.layer.insertSublayer(shape, at: 0)
        infoWindow.layer.shadowColor = UIColor.black.withAlphaComponent(0.60).cgColor
        infoWindow.layer.shadowRadius = 0.5
        infoWindow.layer.shadowOpacity = 0.2
        infoWindow.backgroundColor = .white
        infoWindow.center = mapView.projection.point(for: marker.position)
        infoWindow.center.y = infoWindow.center.y + mapView.frame.height * 0.07
        infoWindow.layer.cornerRadius = 10
        
        mapView.addSubview(infoWindow)
        infoWindow.addSubview(truckNameView)
        infoWindow.addSubview(profileImage)
        infoWindow.addSubview(chevronRight)
        infoWindow.addSubview(checkedinAt)
        infoWindow.addSubview(checkinExpiryField)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapInfoWindow(gestureRecognizer:)))
        gesture.delegate = self
        infoWindow.isUserInteractionEnabled = true
        infoWindow.addGestureRecognizer(gesture)
        
        [
            profileImage.leadingAnchor.constraint(equalTo: infoWindow.leadingAnchor, constant: 10),
            profileImage.heightAnchor.constraint(equalTo: infoWindow.heightAnchor, multiplier: 0.85),
            profileImage.centerYAnchor.constraint(equalTo: infoWindow.centerYAnchor),
            profileImage.widthAnchor.constraint(equalTo: infoWindow.widthAnchor, multiplier: 0.35),
            
            truckNameView.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
            truckNameView.widthAnchor.constraint(equalTo: infoWindow.widthAnchor, multiplier: 0.45),

            checkedinAt.topAnchor.constraint(equalTo: truckNameView.bottomAnchor),
            checkedinAt.centerYAnchor.constraint(equalTo: infoWindow.centerYAnchor),
            checkedinAt.leadingAnchor.constraint(equalTo: truckNameView.leadingAnchor),
            checkedinAt.widthAnchor.constraint(equalTo: infoWindow.widthAnchor, multiplier: 0.45),

            checkinExpiryField.topAnchor.constraint(equalTo: checkedinAt.bottomAnchor),
            checkinExpiryField.leadingAnchor.constraint(equalTo: truckNameView.leadingAnchor),
            checkinExpiryField.widthAnchor.constraint(equalTo: infoWindow.widthAnchor, multiplier: 0.45),

            chevronRight.heightAnchor.constraint(equalToConstant: 20),
            chevronRight.widthAnchor.constraint(equalToConstant: 20),
            chevronRight.centerYAnchor.constraint(equalTo: infoWindow.centerYAnchor),
            chevronRight.leadingAnchor.constraint(equalTo: truckNameView.trailingAnchor)
        ].forEach{ $0.isActive = true }
        
        if let merchant_json = marker.userData as? [String:Any] {
            truckDescription = merchant_json["truck_description"] as? String ?? ""
            truckAddress = merchant_json["address"] as? String ?? ""
            truckName = merchant_json["truck_name"] as? String ?? ""
            checkinExpiry = merchant_json["end"] as? String ?? ""
            truckStatus = merchant_json["status"] as? String ?? ""
            truckCoordinates = marker.position
            
            let dateFromServer = DateFormatter()
            dateFromServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            dateFromServer.timeZone = TimeZone(identifier: "America/New_York")
            let date = dateFromServer.date(from: checkinExpiry)
                        
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            dateFormatter.timeZone = TimeZone.current
            let timeStamp = dateFormatter.string(from: date!)
            checkinExpiryField.text = timeStamp
            
            let truck_image_url = merchant_json["truck_image"] as? String ?? ""
            if truck_image_url != "" {
                Alamofire.request(truck_image_url).validate()
                    .responseData(completionHandler: { responseData in
                        responseData.result.ifSuccess {
                           self.truckImage = UIImage(data: responseData.data!)!
                            profileImage.image = self.truckImage
                        }
                        responseData.result.ifFailure {
                            self.truckImage = UIImage(named: "logo")!
                            profileImage.image = self.truckImage
                        }
                    })
            } else {
                self.truckImage = UIImage(named: "logo")!
                profileImage.image = self.truckImage
            }
        }
        return true
    }
    
    @objc func didTapInfoWindow(gestureRecognizer: UIGestureRecognizer) {
        let truckDetailController = TruckDetailController()
        truckDetailController.truckImage = truckImage
        truckDetailController.truckDescription = truckDescription
        truckDetailController.truckAddress = truckAddress
        truckDetailController.truckName = truckName
        truckDetailController.truckCoordinates = truckCoordinates
        truckDetailController.truckStatus = truckStatus
        self.navigationController?.pushViewController(truckDetailController, animated: true)
    }

}

extension TruckMapViewController : TruckLocationDelegate {
    func truckLocationsUpdated() {
        mapView.clear()
        self.truckMapViewModel.truckLocations.forEach{ location in
            let position = CLLocationCoordinate2D(
                latitude: (location["lat"] as! NSString).doubleValue,
                longitude: (location["lng"] as! NSString).doubleValue
            )
            let marker = GMSMarker(position: position)
            
            let icon = UIImageView(image: UIImage(named: "mapMarker"))
            icon.frame = CGRect(x: 0, y: 0, width: 27.0, height: 35.0)
            marker.iconView = icon
            if var merchant_json = location["merchant"] as? [String:Any] {
                marker.title = merchant_json["truck_name"] as? String
                merchant_json["address"] = location["address"]
                merchant_json["end"] = location["end"]
                merchant_json["status"] = location["status"]
                marker.userData = merchant_json
                marker.map = mapView
            }
        }
    }
}
