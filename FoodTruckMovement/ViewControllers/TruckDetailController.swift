import UIKit
import GoogleMaps
import MapKit

class TruckDetailController: UIViewController {
    var truckImage:UIImage? = nil
    var truckDescription = ""
    var truckAddress = ""
    var truckName = ""
    var truckStatus = ""
    var truckCoordinates: CLLocationCoordinate2D!
    let orange = UIColor(red: 224/255, green: 109/255, blue: 23/255, alpha: 1.0)
    let pink = UIColor(red: 232/255, green: 23/255, blue: 120/255, alpha: 1.0)
    let mapView = GMSMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Food Truck Details"
        
        view.backgroundColor = .white
        
        let truckImageHeight = view.frame.height * 0.25
        let truckImageView = UIImageView(frame: CGRect(x:0, y:0, width: view.frame.width, height: truckImageHeight))
        truckImageView.image = truckImage
        truckImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(truckImageView)
        truckImageView.clipsToBounds = true
        truckImageView.contentMode = .scaleAspectFill
        
        let opactityLayer = CALayer()
        opactityLayer.frame = truckImageView.bounds
        opactityLayer.backgroundColor = UIColor.black.cgColor
        truckImageView.layer.addSublayer(opactityLayer)
        opactityLayer.opacity = 0.60
        
        let truckLabel = MyCATextLayer()
        truckLabel.frame = truckImageView.bounds
        truckLabel.isWrapped = true
        truckLabel.string = truckName
        truckLabel.font = UIFont(name:"OpenSans-Regular", size:25)
        truckLabel.alignmentMode = .center
        truckLabel.contentsScale = UIScreen.main.scale
        truckImageView.layer.addSublayer(truckLabel)
        
        let truckQuickInfo = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.04))
        truckQuickInfo.isUserInteractionEnabled = false
        let truckQuickInfoBounds = truckQuickInfo.bounds

        let truckQuickInfogradient = CAGradientLayer()
        truckQuickInfogradient.frame = truckQuickInfoBounds
        truckQuickInfogradient.colors = [orange.cgColor, pink.cgColor]
        truckQuickInfogradient.startPoint = CGPoint(x: 0, y: -2.0)
        truckQuickInfogradient.endPoint = CGPoint(x: 0, y: 1.25)
        truckQuickInfo.layer.insertSublayer(truckQuickInfogradient, at: 0)
        truckQuickInfo.layer.shadowColor = UIColor.black.cgColor
        truckQuickInfo.layer.shadowOpacity = 0.2
        truckQuickInfo.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        truckQuickInfo.layer.shadowRadius = 5
        truckQuickInfo.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(truckQuickInfo)
        
        let truckAddressView = UITextField()
        truckAddressView.isUserInteractionEnabled = false
        truckAddressView.text = truckAddress
        truckAddressView.font = UIFont(name:"OpenSans-Regular", size:13)
        truckAddressView.backgroundColor = .clear
        truckAddressView.textColor = .white
        truckAddressView.textAlignment = .center
        truckAddressView.translatesAutoresizingMaskIntoConstraints = false
        truckQuickInfo.addSubview(truckAddressView)
        
        let truckLocationPin = UIImageView()
        truckLocationPin.image = UIImage(named: "outline-place-white")
        truckLocationPin.tintColor = .white
        truckLocationPin.translatesAutoresizingMaskIntoConstraints = false
        truckQuickInfo.addSubview(truckLocationPin)
        
        
        let contentScrollView = UIScrollView()
        contentScrollView.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.isScrollEnabled = true
        contentScrollView.isUserInteractionEnabled = true
        view.addSubview(contentScrollView)

        let inset = view.frame.width * 0.05

        let truckStatusView = UITextView()
        truckStatusView.text = truckStatus
        truckStatusView.isEditable = false
        truckStatusView.isSelectable = false
        truckStatusView.font = UIFont(name:"OpenSans-Regular", size:13)
        truckStatusView.textColor = .gray
        truckStatusView.textContainerInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        truckStatusView.translatesAutoresizingMaskIntoConstraints = false
        truckStatusView.sizeToFit()
        truckStatusView.isScrollEnabled = false
        contentScrollView.addSubview(truckStatusView)
        
        let HorizontalRule = UIView(frame: CGRect(x: 0, y: 100, width: 320, height: 1))
        HorizontalRule.layer.borderWidth = 1.0
        HorizontalRule.layer.borderColor = UIColor.black.cgColor
        HorizontalRule.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.addSubview(HorizontalRule)

        HorizontalRule.backgroundColor = .black
        contentScrollView.addSubview(HorizontalRule)
                        
        let truckDescriptionHeader = UITextField()
        truckDescriptionHeader.isUserInteractionEnabled = false
        truckDescriptionHeader.text = "About " + truckName
        truckDescriptionHeader.font = UIFont(name:"OpenSans-Bold", size:14)
        truckDescriptionHeader.backgroundColor = .clear
        truckDescriptionHeader.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.addSubview(truckDescriptionHeader)
        
        let truckDescriptionView = UITextView()
        truckDescriptionView.text = truckDescription
        truckDescriptionView.isEditable = false
        truckDescriptionView.isSelectable = false
        truckDescriptionView.isScrollEnabled = false
        truckDescriptionView.font = UIFont(name:"OpenSans-Regular", size:13)
        truckDescriptionView.textColor = .gray
        truckDescriptionView.textContainerInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        truckDescriptionView.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.addSubview(truckDescriptionView)
        
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: darkMapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        mapView.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.addSubview(mapView)
        
        mapView.camera = GMSCameraPosition.init(latitude: truckCoordinates.latitude,
                                                longitude: truckCoordinates.longitude,
                                                zoom: 16.0)
        let marker = GMSMarker(position: truckCoordinates)
        
        let icon = UIImageView(image: UIImage(named: "mapMarker"))
        icon.frame = CGRect(x: 0, y: 0, width: 16.0, height: 22.0)
        marker.iconView = icon
        marker.map = mapView
        
        contentScrollView.bringSubviewToFront(truckQuickInfo)
        view.bringSubviewToFront(contentScrollView)

        [
            truckImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            truckImageView.widthAnchor.constraint(equalToConstant: view.frame.width),
            truckImageView.heightAnchor.constraint(equalToConstant: truckImageHeight),
            
            truckQuickInfo.topAnchor.constraint(equalTo: truckImageView.bottomAnchor),
            truckQuickInfo.widthAnchor.constraint(equalToConstant: truckQuickInfoBounds.width),
            truckQuickInfo.heightAnchor.constraint(equalToConstant: truckQuickInfoBounds.height),
            
            truckAddressView.centerYAnchor.constraint(equalTo: truckQuickInfo.centerYAnchor),
            truckAddressView.centerXAnchor.constraint(equalTo: truckQuickInfo.centerXAnchor),
            
            truckLocationPin.trailingAnchor.constraint(equalTo: truckAddressView.leadingAnchor),
            truckLocationPin.centerYAnchor.constraint(equalTo: truckQuickInfo.centerYAnchor),
            
            contentScrollView.topAnchor.constraint(equalTo: truckQuickInfo.bottomAnchor),
            contentScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            truckStatusView.topAnchor.constraint(equalTo: contentScrollView.topAnchor),
            truckStatusView.widthAnchor.constraint(equalToConstant: view.frame.width),
            
            HorizontalRule.heightAnchor.constraint(equalToConstant: 1),
            HorizontalRule.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: inset),
            HorizontalRule.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -inset),
            HorizontalRule.topAnchor.constraint(equalTo: truckStatusView.bottomAnchor, constant: 10),
            
            truckDescriptionHeader.topAnchor.constraint(equalTo: HorizontalRule.bottomAnchor, constant: 20),
            truckDescriptionHeader.widthAnchor.constraint(equalToConstant: view.frame.width),
            truckDescriptionHeader.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: inset),
                        
            truckDescriptionView.topAnchor.constraint(equalTo: truckDescriptionHeader.bottomAnchor),
            truckDescriptionView.heightAnchor.constraint(greaterThanOrEqualToConstant: view.frame.height * 0.15),
            truckDescriptionView.widthAnchor.constraint(equalToConstant: view.frame.width),
            
            mapView.widthAnchor.constraint(equalToConstant: view.frame.width),
            mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mapView.topAnchor.constraint(equalTo: truckDescriptionView.bottomAnchor),
            mapView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.3),
            mapView.bottomAnchor.constraint(equalTo: contentScrollView.bottomAnchor)

        ].forEach{ $0.isActive = true }
        
        truckStatusView.sizeToFit()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let showRouteButton = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        showRouteButton.setTitle("SHOW ROUTE", for: .normal)
        showRouteButton.layer.cornerRadius = mapView.frame.height * 0.15 / 2
        showRouteButton.backgroundColor = .white
        showRouteButton.setTitleColor(.gray, for: .normal)
        showRouteButton.titleLabel?.font = UIFont(name:"OpenSans-Regular", size:14)
        showRouteButton.translatesAutoresizingMaskIntoConstraints = false
        
        showRouteButton.addTarget(self, action: #selector(openInMaps), for: .touchUpInside)
        
        mapView.addSubview(showRouteButton)
        
        [
            showRouteButton.heightAnchor.constraint(equalTo: mapView.heightAnchor, multiplier: 0.15),
            showRouteButton.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -50.0),
            showRouteButton.widthAnchor.constraint(equalTo: mapView.widthAnchor, multiplier: 0.6),
            showRouteButton.centerXAnchor.constraint(equalTo: mapView.centerXAnchor)
        ].forEach{ $0.isActive = true }
    }
    
    @objc func openInMaps(sender: UIButton!) {
        let latitude: CLLocationDegrees = truckCoordinates.latitude
        let longitude: CLLocationDegrees = truckCoordinates.longitude
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: "comgooglemaps://?center=\(latitude),\(longitude)&zoom=14&views=traffic&q=loc:\(latitude),\(longitude)")!, options: [:], completionHandler: nil)
        } else {
            print("Can't use comgooglemaps://")
            UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(latitude),\(longitude)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}


class MyCATextLayer: CATextLayer {
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(layer: aDecoder)
    }
    
    override func draw(in ctx: CGContext) {
        let height = self.bounds.size.height
        let fontSize = self.fontSize
        let yDiff = (height-fontSize)/2 - fontSize/10
        
        ctx.saveGState()
        ctx.translateBy(x: 0.0, y: yDiff)
        super.draw(in: ctx)
        ctx.restoreGState()
    }
}
