import UIKit
import Foundation

class InfoPageController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        self.title = "Food Truck Movement"
        
        let info = UILabel()
        info.text = "No trucks nearby? Help us change that!"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info.numberOfLines = 0
        view.addSubview(info)
        info.translatesAutoresizingMaskIntoConstraints = false
        
        let info2 = UILabel()
        info2.text = "We're a small group of friends based in Portland, Maine with a dream of making finding food trucks easier for everybody, but we need your help to do it. Let your favorite trucks know about our app so they can register for an account and start updating their location!"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info2.numberOfLines = 0
        view.addSubview(info2)
        info2.translatesAutoresizingMaskIntoConstraints = false
        
        let info3 = UILabel()
        info3.text = "Follow us on social media for the latest updates!"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info3.numberOfLines = 0
        view.addSubview(info3)
        info3.translatesAutoresizingMaskIntoConstraints = false
        
        let info4 = UILabel()
        info4.text = "Twitter: @FTM_App"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info4.numberOfLines = 0
        view.addSubview(info4)
        info4.translatesAutoresizingMaskIntoConstraints = false
        
        let info5 = UILabel()
        info5.text = "Instagram: food_truck_movement"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info5.numberOfLines = 0
        view.addSubview(info5)
        info5.translatesAutoresizingMaskIntoConstraints = false
        
        let info6 = UILabel()
        info6.text = "Facebook: FTMovement"
        info.font = UIFont(name:"OpenSans-Regular", size:17)
        info6.numberOfLines = 0
        view.addSubview(info6)
        info6.translatesAutoresizingMaskIntoConstraints = false


        [
            info.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            info.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            info2.topAnchor.constraint(equalTo: info.bottomAnchor, constant: 30),
            info2.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info2.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            info3.topAnchor.constraint(equalTo: info2.bottomAnchor, constant: 30),
            info3.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info3.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            info4.topAnchor.constraint(equalTo: info3.bottomAnchor, constant: 10),
            info4.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info4.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            info5.topAnchor.constraint(equalTo: info4.bottomAnchor, constant: 10),
            info5.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info5.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            info6.topAnchor.constraint(equalTo: info5.bottomAnchor, constant: 10),
            info6.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            info6.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
        ].forEach{ $0.isActive = true }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
