//
//  AppDelegate.swift
//  FoodTruckMovement
//
//  Created by Julian Gallo on 6/23/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBevcSQoXcROukkMeworCzUY_Onle5e5pA")
        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            let truckMapViewController = TruckMapViewController()
            navigationController = UINavigationController(rootViewController: truckMapViewController)
            window.rootViewController = navigationController
            
            let orange = UIColor(red: 224/255, green: 109/255, blue: 23/255, alpha: 1.0)
            let pink = UIColor(red: 232/255, green: 23/255, blue: 120/255, alpha: 1.0)
            
            if let navigationBar = navigationController?.navigationBar {
                
                let gradient = CAGradientLayer()
                var bounds = navigationBar.bounds
                bounds.size.height += UIApplication.shared.statusBarFrame.size.height
                gradient.frame = bounds
                gradient.colors = [orange.cgColor, pink.cgColor]
                gradient.startPoint = CGPoint(x: 0, y: -1.0)
                gradient.endPoint = CGPoint(x: 0, y: 1.5)
                
                var gradientImage:UIImage?
                UIGraphicsBeginImageContext(gradient.frame.size)
                if let context = UIGraphicsGetCurrentContext() {
                    gradient.render(in: context)
                    gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
                }
                UIGraphicsEndImageContext()
                
                navigationBar.setBackgroundImage(gradientImage, for: UIBarMetrics.default)
                
                navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
                navigationBar.titleTextAttributes = [
                    NSAttributedString.Key.foregroundColor : UIColor.white,
                    NSAttributedString.Key.font: UIFont(name:"OpenSans-Regular", size:16)!
                ]
                navigationBar.tintColor = .white
            }
            
            window.makeKeyAndVisible()
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

