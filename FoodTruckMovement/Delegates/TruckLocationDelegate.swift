import Foundation

@objc protocol TruckLocationDelegate {
    func truckLocationsUpdated()
}
