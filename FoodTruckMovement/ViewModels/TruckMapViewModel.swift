import Alamofire
import UIKit
import MapKit

class TruckMapViewModel: NSObject {
    
    weak var delegate: TruckLocationDelegate? = nil
    var truckLocations : [Dictionary<String,Any>]!
    
    func getTruckLocations(southWest: CLLocationCoordinate2D, northEast: CLLocationCoordinate2D) {
        let parameters : [String:Any] = ["sw_lat": southWest.latitude, "sw_long": southWest.longitude,
                                         "ne_lat": northEast.latitude, "ne_long": northEast.longitude]
        Alamofire.request("\(backend_server_url)/api/location/", method: .get, parameters: parameters).responseJSON { response in
            do {
                if let data = response.data, let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                    self.truckLocations = jsonArray
                    self.delegate?.truckLocationsUpdated()
                }
            } catch {
                print(error)
            }
        }
    }
    
}


