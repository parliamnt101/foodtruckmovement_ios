import UIKit
import GoogleMaps

class TruckMapView {
    
    func initializeTruckMapView(view: UIView) {
        view.backgroundColor = .white
    }
    
    func addMapView(parentView: UIView) -> GMSMapView {
        let mapView = GMSMapView()
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(mapView)
        [
            mapView.topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor),
            mapView.bottomAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.bottomAnchor),
            mapView.leadingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.trailingAnchor)
        ].forEach{ $0.isActive = true }
        
        return mapView
    }
    
    func initializeTruckMarkerIcon(markerIcon: UIImageView) {
        markerIcon.image = UIImage(named: "mapMarker")
    }

    
}
